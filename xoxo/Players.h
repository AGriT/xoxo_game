#pragma once

enum eTypeOfCells
{
	eTOC_Empty			,
	eTOC_X				,
	eTOC_0
};

class CPlayers
{
public:
	CPlayers(eTypeOfCells eTOCCurrent = eTypeOfCells::eTOC_Empty);
	virtual ~CPlayers();

	virtual void Drawning();

	eTypeOfCells getType();

protected:
	eTypeOfCells m_eTOCCurrent;
};

