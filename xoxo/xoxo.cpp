// xoxo.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <stdio.h>
#include <iostream>
#include <clocale>
#include "xoxo.h"

using namespace std;
static const int sc_nSquareLenght = 3;
static const int sc_nWinningLenght = 3;
char cells[sc_nSquareLenght][sc_nSquareLenght];

void logicSingleBot()
{
	//�������� 3 � ��� �� ����������� � ���������
	int i = 0;
	int j = 0;
	for (i = 0; i < sc_nSquareLenght; i++)
	{
		for (j = 0; j < sc_nSquareLenght - sc_nWinningLenght + 1; j++)
		{
			if (cells[i][j] == cells[i][j + 1] && cells[i][j] != ' ' && cells[j][j + 2] == ' ')
			{
				cells[i][j + 2] = '0';
				return;
			}

			if (cells[i][j] == cells[i][j + 2] && cells[i][j] != ' ' && cells[i][j + 1] == ' ')
			{
				cells[i][j + 1] = '0';
				return;
			}

			if (cells[i][j + 2] == cells[i][j + 1] && cells[i][j + 2] != ' ' && cells[i][j] == ' ')
			{
				cells[i][j] = '0';
				return;
			}

			if (cells[j][i] == cells[j + 1][i] && cells[j][i] != ' ' && cells[j + 2][i] == ' ')
			{
				cells[2][i] = '0';
				return;
			}

			if (cells[j][i] == cells[j + 2][i] && cells[j][i] != ' ' && cells[j + 1][i] == ' ')
			{
				cells[j + 1][i] = '0';
				return;
			}

			if (cells[j + 2][i] == cells[j + 1][i] && cells[j + 2][i] != ' ' && cells[j][i] == ' ')
			{
				cells[j][i] = '0';
				return;
			}
		}
	}

	//������������ ��������

	if (cells[0][0] == cells[1][1] && cells[0][0] != ' ' && cells[2][2] == ' ')
	{
		cells[2][2] = '0';
		return;
	}

	if (cells[0][0] == cells[2][2] && cells[0][0] != ' ' && cells[1][1] == ' ')
	{
		cells[1][1] = '0';
		return;
	}

	if (cells[2][2] == cells[1][1] && cells[2][2] != ' ' && cells[0][0] == ' ')
	{
		cells[0][0] = '0';
		return;
	}

	if (cells[0][2] == cells[1][1] && cells[0][2] != ' ' && cells[2][0] == ' ')
	{
		cells[2][0] = '0';
		return;
	}

	if (cells[0][2] == cells[2][0] && cells[0][2] != ' ' && cells[1][1] == ' ')
	{
		cells[1][1] = '0';
		return;
	}

	if (cells[2][0] == cells[1][1] && cells[2][0] != ' ' && cells[0][2] == ' ')
	{
		cells[0][2] = '0';
		return;
	}

	// �������� ������

	if (cells[1][1] == ' ')
	{
		cells[1][1] = '0';
		return;
	}

	//���������� �������

	for (i = 0; i < sc_nSquareLenght; i++)
	{
		for (j = 0; j < sc_nSquareLenght; j++)
		{
			if (cells[i][j] == ' ')
			{
				cells[i][j] = '0';
				return;
			}
		}
	}
}

char finishCheck()
{
	char cSymbolHor = ' ';
	char cSymbolVert = ' ';
	char cWinningSymbol = ' ';
	int nCounterHor = 0;
	int nCounterVert = 0;
	int i, j;

	// ������ ����������� � ���������
	for (i = 0; i < sc_nSquareLenght; i++)
	{
		nCounterHor = 0;
		nCounterVert = 0;

		for (j = 0; j < sc_nSquareLenght; j++)
		{
			if (nCounterHor == sc_nWinningLenght || nCounterVert == sc_nWinningLenght)
			{
				if (nCounterHor == sc_nWinningLenght)
					return cSymbolHor;

				return cSymbolVert;
			}

			if (cells[i][j] != ' ' && cells[i][j] == cSymbolHor)
				nCounterHor++;
			
			if (cells[j][i] != ' ' && cells[j][i] == cSymbolVert)
				nCounterVert++;

			if (cells[i][j] != cSymbolHor)
			{
				cSymbolHor = cells[i][j];
				nCounterHor = 1;
			}
			
			if (cells[j][i] != cSymbolVert)
			{
				cSymbolVert = cells[j][i];
				nCounterVert = 1;
			}
		}
	}

	//������ �� ���������
	for (i = 0; i < sc_nSquareLenght; i++)
	{
		char cSymbolDiag = ' ';
		char cSymbolReverseDiag = ' ';
		int nCounterDiag = 0;
		int nCounterReverseDiag = 0;

		if (nCounterDiag == sc_nWinningLenght || nCounterReverseDiag == sc_nWinningLenght)
		{
			if (nCounterDiag == sc_nWinningLenght)
				return cSymbolDiag;

			return cSymbolReverseDiag;
		}

		if (cells[i][i] != ' ' && cells[i][i] == cSymbolDiag)
			nCounterDiag++;

		if (cells[i][sc_nSquareLenght - i] != ' ' && cells[i][sc_nSquareLenght - i] == cSymbolReverseDiag)
			nCounterReverseDiag++;

		if (cells[i][i] != cSymbolDiag)
		{
			cSymbolDiag = cells[i][i];
			nCounterDiag = 1;
		}

		if (cells[i][sc_nSquareLenght - i] != cSymbolReverseDiag)
		{
			cSymbolReverseDiag = cells[i][sc_nSquareLenght - i];
			nCounterReverseDiag = 1;
		}
	}

	return ' ';
}

void xoxoGame()
{
	system("cls");

	int i = 0, j = 0;
	int nChosenPlayer = 0, nSingleMulti = 0, nMoves = 0;
	int nVert = 0, nHor = 0;

	// ������� �������

	for (i = 0; i < sc_nSquareLenght; i++)
	{
		for (j = 0; j < sc_nSquareLenght; j++)
			cells[i][j] = ' ';
	}

	for (i = 0; i != 1;)
	{
		printf("�������� �����: 1 ����� - 1, 2 ������ - 2\n");
		scanf_s("%d", &nSingleMulti);
		printf("��� ����� ������? x - 1, 0 - 2\n");
		scanf_s("%d", &nChosenPlayer);
		i = 1;

		if ((nChosenPlayer != 1 && nChosenPlayer != 2) || (nSingleMulti != 1 && nSingleMulti != 2))
		{
			printf("���� �����������. ��������� ����.\n");
			i = 0;
		}
	}

	for (;;)
	{
		system("cls");

		//��������� ����

		i = 0;

		for (j = 1; j <= 11; j++)
		{
			if (j % 4 == 0)
				printf("___ ___ ___\n");
			else if ((j - 2) % 4 == 0)
			{
				printf(" %c | %c | %c \n", cells[i][0], cells[i][1], cells[i][2]);
				i++;
			}
			else
				printf("   |   |   \n");
		}

		//�������� ��������
		char cWinningSymbol = finishCheck();

		if (cWinningSymbol == 'x')
		{
			printf("������ x!\n");
			break;
		}

		if (cWinningSymbol == '0')
		{
			printf("������ 0!\n");
			break;
		}

		if (cWinningSymbol == ' ')
		{
			for (i = 0; i < sc_nSquareLenght; i++)
			{
				for (j = 0; j < sc_nSquareLenght; j++)
				{
					if (cells[i][j] == ' ')
						break;

				}
			}

			if (i == sc_nSquareLenght && j == sc_nSquareLenght)
			{
				printf("�����\n");
				break;
			}
		}

		//������� ��� ���
		if (nSingleMulti != 1)
		{
			if (nChosenPlayer == 1)
				printf("��� �. ������� ������ � �������\n");
			else
				printf("��� 0. ������� ������ � �������\n");
		}
		else if (nChosenPlayer == 1)
			printf("��� ���. ������� ������ � �������\n");
		else
		{
			logicSingleBot();
			nChosenPlayer = 1;
			continue;
		}



		//��������� ����
		for (;;)
		{
			scanf_s("%d %d", &nHor, &nVert);
			--nHor;
			--nVert;

			//������ �������� � ������
			if (cells[nHor][nVert] != ' ')
			{
				printf("��������� ������ ��� ������, ���������� �����.\n");
				continue;
			}

			if (nChosenPlayer == 1)
				cells[nHor][nVert] = 'x';
			else
				cells[nHor][nVert] = '0';

			break;
		}


		if (nChosenPlayer == 1)
			nChosenPlayer = 2;
		else
			nChosenPlayer = 1;
	}

	return;
}

int main()
{
	setlocale(LC_CTYPE, "rus");
	int nExit = 0;

	do
	{
		xoxoGame();

		printf("���� ��������. ��� ������ ����� ���� ������� 1\n");
		scanf_s("%d", &nExit);

	} while (nExit == 1);

	system("pause");
	return 0;
}