#pragma once
#include "Players.h"

class CPlayBoard
{
public:
	CPlayBoard();
	~CPlayBoard();

	void Drawning();
	bool isWinning();
	void Paste(char x, char y, CPlayers* pCurrentPlayer);
};

