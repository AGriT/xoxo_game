#pragma once
#include "Players.h"
#include "PlayBoard.h"

class CBotPlayer :
	public CPlayers
{
public:
	CBotPlayer(eTypeOfCells eTOCCurrent);
	~CBotPlayer();

	virtual void Drawning();

	static void NextMove(CPlayBoard& rCurrentBoard);
};

