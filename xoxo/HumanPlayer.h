#pragma once
#include "Players.h"
#include "PlayBoard.h"

class CHumanPlayer :
	public CPlayers
{
public:
	CHumanPlayer(eTypeOfCells eTOCCurrent);
	~CHumanPlayer();

	virtual void Drawning();

	static void NextMove(CPlayBoard& rCurrentBoard);
};

